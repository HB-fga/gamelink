Rails.application.routes.draw do

  resources :offereds
  resources :wanteds
  devise_for :users
  resources :games
  resources :users

  get 'offereds/index'
  root 'offereds#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/rrootouting.html
end
