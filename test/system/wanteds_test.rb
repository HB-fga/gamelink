require "application_system_test_case"

class WantedsTest < ApplicationSystemTestCase
  setup do
    @wanted = wanteds(:one)
  end

  test "visiting the index" do
    visit wanteds_url
    assert_selector "h1", text: "Wanteds"
  end

  test "creating a Wanted" do
    visit wanteds_url
    click_on "New Wanted"

    fill_in "Game", with: @wanted.game_id
    fill_in "User", with: @wanted.user_id
    click_on "Create Wanted"

    assert_text "Wanted was successfully created"
    click_on "Back"
  end

  test "updating a Wanted" do
    visit wanteds_url
    click_on "Edit", match: :first

    fill_in "Game", with: @wanted.game_id
    fill_in "User", with: @wanted.user_id
    click_on "Update Wanted"

    assert_text "Wanted was successfully updated"
    click_on "Back"
  end

  test "destroying a Wanted" do
    visit wanteds_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Wanted was successfully destroyed"
  end
end
