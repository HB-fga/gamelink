require "application_system_test_case"

class OfferedsTest < ApplicationSystemTestCase
  setup do
    @offered = offereds(:one)
  end

  test "visiting the index" do
    visit offereds_url
    assert_selector "h1", text: "Offereds"
  end

  test "creating a Offered" do
    visit offereds_url
    click_on "New Offered"

    fill_in "Game", with: @offered.game_id
    fill_in "User", with: @offered.user_id
    click_on "Create Offered"

    assert_text "Offered was successfully created"
    click_on "Back"
  end

  test "updating a Offered" do
    visit offereds_url
    click_on "Edit", match: :first

    fill_in "Game", with: @offered.game_id
    fill_in "User", with: @offered.user_id
    click_on "Update Offered"

    assert_text "Offered was successfully updated"
    click_on "Back"
  end

  test "destroying a Offered" do
    visit offereds_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Offered was successfully destroyed"
  end
end
