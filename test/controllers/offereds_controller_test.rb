require 'test_helper'

class OfferedsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @offered = offereds(:one)
  end

  test "should get index" do
    get offereds_url
    assert_response :success
  end

  test "should get new" do
    get new_offered_url
    assert_response :success
  end

  test "should create offered" do
    assert_difference('Offered.count') do
      post offereds_url, params: { offered: { game_id: @offered.game_id, user_id: @offered.user_id } }
    end

    assert_redirected_to offered_url(Offered.last)
  end

  test "should show offered" do
    get offered_url(@offered)
    assert_response :success
  end

  test "should get edit" do
    get edit_offered_url(@offered)
    assert_response :success
  end

  test "should update offered" do
    patch offered_url(@offered), params: { offered: { game_id: @offered.game_id, user_id: @offered.user_id } }
    assert_redirected_to offered_url(@offered)
  end

  test "should destroy offered" do
    assert_difference('Offered.count', -1) do
      delete offered_url(@offered)
    end

    assert_redirected_to offereds_url
  end
end
