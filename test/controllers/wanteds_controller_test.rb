require 'test_helper'

class WantedsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @wanted = wanteds(:one)
  end

  test "should get index" do
    get wanteds_url
    assert_response :success
  end

  test "should get new" do
    get new_wanted_url
    assert_response :success
  end

  test "should create wanted" do
    assert_difference('Wanted.count') do
      post wanteds_url, params: { wanted: { game_id: @wanted.game_id, user_id: @wanted.user_id } }
    end

    assert_redirected_to wanted_url(Wanted.last)
  end

  test "should show wanted" do
    get wanted_url(@wanted)
    assert_response :success
  end

  test "should get edit" do
    get edit_wanted_url(@wanted)
    assert_response :success
  end

  test "should update wanted" do
    patch wanted_url(@wanted), params: { wanted: { game_id: @wanted.game_id, user_id: @wanted.user_id } }
    assert_redirected_to wanted_url(@wanted)
  end

  test "should destroy wanted" do
    assert_difference('Wanted.count', -1) do
      delete wanted_url(@wanted)
    end

    assert_redirected_to wanteds_url
  end
end
