# GameLink

## Participantes
- Hugo Ricardo Souza Bezerra - 18/0113585 - Turma: Carla

- Lucas Gabriel Bezerra - 18/0125770 - Turma: Carla

## Dependências
Aplicação testada e funcionando de maneira estavel nas seguintes dependências:
- ruby 2.5.1p57 (2018-03-29 revision 63029) [x86_64-linux-gnu]
    - rails 6.0.1
- yarn 1.19.1
- sqlite3 3.22.0

## Como Executar  
Após clonar e obter o repositório na sua maquina:  
- acesse o diretório gamelink no seu terminal
- execute os comandos:  
    
    ``` 
        $ bundle install
        $ yarn install --check-files
        $ rails db:drop
        $ rails db:setup
        $ rails server
    ```

- abra o seu navegador e acesse: http://localhost:3000/

## Como acessar o Usuário administrador
- login: admin@mail.com
- senha: admin123

## Resumo da Proposta
Os impostos sobre jogos eletrônicos no Brasil são muito altos e isso torna toda essa mídia menos acessível para nós, brasileiros. A proposta do GameLink é criar um local onde você possa ***oferecer os jogos que não joga mais*** para as pessoas que tem interesse em jogá-los e ***adquirir jogos oferecidos*** por essas mesmas pessoas, unindo a comunidade gamer e possibilitando que os usuários joguem uma maior variedade de jogos por meio de ***trocas e empréstimos***, tornando esse hobby mais econômico

## Funcionalidades
- Cadastro e Login de ***Usuário padrão***
    - Contato
    - ***Gerenciamento de lista de desejos***
    - ***Gerenciamento de lista de ofertas***
- Login de ***Usuário administrador:***
    - Cria novos jogos no banco de dados
    - Edita jogos do banco de dados
    - Deleta jogos do banco de dados
- Pesquisa de Jogos no banco de dados
- Vizualizacao de Jogos ofertados pela comunidade
- Acesso ao perfil de outros usuarios