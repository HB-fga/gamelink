class Game < ApplicationRecord
  has_many :offereds, dependent: :destroy
  has_many :users, through: :offereds
  has_many :wanteds, dependent: :destroy
  has_many :users, through: :wanteds
  validates :title, presence: :true, uniqueness: { case_sensitive: false }

end
