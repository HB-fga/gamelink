class User < ApplicationRecord
  has_many :offereds, dependent: :destroy
  has_many :users, through: :offereds
  has_many :wanteds, dependent: :destroy
  has_many :users, through: :wanteds
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  validates :username, presence: :true, uniqueness: { case_sensitive: false }
  validates_format_of :username, with: /^[a-z0-9_]*$/, :multiline => true
  extend FriendlyId
  friendly_id :username, use: :slugged

end
