class OfferedsController < ApplicationController
  before_action :set_offered, only: [:show, :edit, :update, :destroy]

  # GET /offereds
  # GET /offereds.json
  def index
    @offereds = Offered.all
  end

  # GET /offereds/1
  # GET /offereds/1.json
  def show
  end

  # GET /offereds/new
  def new
    @offered = Offered.new
  end

  # GET /offereds/1/edit
  def edit
  end

  # POST /offereds
  # POST /offereds.json
  def create
    @offered = Offered.new(offered_params)

    respond_to do |format|
      if @offered.save
        format.html { redirect_to "/games", notice: 'Jogo adicionado a lista de ofertados' }
        format.json { render :show, status: :created, location: @offered }
      else
        format.html { render :new }
        format.json { render json: @offered.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /offereds/1
  # PATCH/PUT /offereds/1.json
  def update
    respond_to do |format|
      if @offered.update(offered_params)
        format.html { redirect_to @offered, notice: 'Jogo atualizado aos ofertados com sucesso.' }
        format.json { render :show, status: :ok, location: @offered }
      else
        format.html { render :edit }
        format.json { render json: @offered.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /offereds/1
  # DELETE /offereds/1.json
  def destroy
    @offered.destroy
    respond_to do |format|
      format.html { redirect_to "/users/" + current_user.username, notice: 'Jogo retirado da lista de ofertados' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_offered
      @offered = Offered.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def offered_params
      params.require(:offered).permit(:user_id, :game_id)
    end
end
