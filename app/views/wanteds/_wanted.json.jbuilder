json.extract! wanted, :id, :user_id, :game_id, :created_at, :updated_at
json.url wanted_url(wanted, format: :json)
