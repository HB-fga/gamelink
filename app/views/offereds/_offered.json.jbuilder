json.extract! offered, :id, :user_id, :game_id, :created_at, :updated_at
json.url offered_url(offered, format: :json)
